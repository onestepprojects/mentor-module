# Example Module

This serves as an example module for the One Step Relief Platform. You should be able to get something polished fairly quickly with this code.

## Structure

The module is made up of 3 main directories: `frontend/`, `backend/`, and `deploy`. 

### Frontend

The `frontend` directory contains code for a "Hello World" React application, as well as starter-code for making api calls to existing backends. It also includes code for aws-amplify and Cognito. For more information about developing the frontend, please go [here](./frontend/README).

### Backend

The `backend` directory containers code for a "Hello World" Dropwizard application. It also has a `Dockerfile` that can be used for testing and deployment.

### Deployment

The `deploy` directory contains a helm chart that can be used to stand up the application in Kubernetes.

## Development Lifecycle
### CI/CD

CI/CD configuration can be found in the `gitlab-ci.yml` file, as well as the [templates repository](https://gitlab.com/onestepprojects/ci-templates). The `docker build` command may need to be updated if environment variables should be included in the Docker image.

The below diagram depicts the CI/CD Pipeline process:

![Pipeline Diagram](docs/images/pipeline.png)

### Branching Model

We will be using a branching model as described in the example diagrams below. Capstone students should do all work off of the `capstone-main` branch in each repository. They should also format their branch names with the following convention: `<type>/<username>/capstone/<description>`.

![General Branching Diagram](docs/images/branching.png)

![Example Branching Diagram](docs/images/example-branching.png)

## Modifying the Example Module for New Module Creation

Development of a module can begin by copying this Example Module to a new repository.

In order to begin development based on this example modules, you will immediately want to make some changes. This will also act as an opportunity to get familiar with the development lifecycle.

Follow the steps below to achieve this.

1. Make sure that you are on the `capstone-main` branch if you are a capstone student. (`git checkout capstone-main`)

1. Checkout a new branch with the following command (with variables changed and assuming you are a capstone student).

`git checkout -b feature/<username>/capstone-main/change-relevant-module-names`

1. In the `gitlab-ci.yml` file...

    1. Delete lines 12-21. This prevents the pipeline from running and is not necessary.

    1. Change the "GITLAB_IMAGE" variable in the "build-services" job to `${CI_REGISTRY}/${CI_PROJECT_PATH}/<desired_name_of_backend_service>`

1. In the `backend` directory...

    1. Change the names of the two files under `src/main/java/org/onestep/relief/app/` (`ExampleServiceApplication.java` and `ExampleServiceConfiguration.java`) to include your service name.

    1. Change all instances of `ExampleServiceApplication` and `ExampleServiceConfiguration` in the same two files.

    1. Change the content in the file `src/main/resources/assets/banner.txt` to match your service name.

    1. In the `Dockerfile`, change lines 11, 12, and 14, to reference a `jar` file with the correct name.

    1. In the `pom.xml` file, change line 10 to represent the same name as in the `Dockerfile`, and line 14 to match the same name as the `banner.txt` file.

1. In the `deploy` directory...

    1. Change the name of the directory `helm/example` to be `helm/<same_name_as_in_gitlab_image>`

    1. In all the files in the directory `helm/<same_name_as_in_gitlab_image>`, change all instances of "example" in these files to the same name as the changed directory name ("same_name_as_in_gitlab_image"). There are changes to be made in the following filenames:

        * `deployment.yaml`
        * `ingress.yaml`
        * `service.yaml`
        * `prod.yaml`
        * `Chart.yaml`

    1. In the file `helm/<same_name_as_in_gitlab_image>/values.yaml`, set the "repository" field on line 4 to be `registry.gitlab.com/onestepprojects/<repo_slug_name>/<desired_name_of_backend_service>`. The nane of the backend service should be the same as what you changed in the `gitlab-ci.yml` file for the image name.

1. When you replace the content in this `README.md` file, we recomment removing the images in `docs/images` as well; however, you may want to continue to use this directory for other artifacts of documentation.

1. In the `frontend` directory...

    1. Create a file called `.env.development.local`. Copy the file for the UI 

    1. A lot of the code is still relevant and can be left. It sets up redux and a call to the OPP Module Service to get the logged in user. You will probably need this code.

    1. In the file `src/App.tsx` and `src/Container.tsx` change the following references to match your new module name:
        * `ExampleContainer`
        * `ExampleContainerProps`
        * `ExampleModuleProps`
        * `ExampleModule`

    1. In the file, `demo.tsx`, change the path variable on line 85 to be the path that you want your UI to be available at in the platform.

    1. In the file, `index.html`, change the title on line 8 to be relevant to your module.

    1. In the file, `package.json`...
        1. Change the name on line 2 represent your module. This will be the package name variable.

        1. Change line 4 to a description that represents your module.

        1. Change line 7 to the url of your repository.

    1. Run `cd frontend/` if not already there, and then run `npm install`, This will create a `package-lock.json` file.

1. You are now ready to push code for the first time. Run the following commands:

```bash
git add .
git commit -m "feature(module): Make relevant changes to replace example module with <module_name> Module."
git push
```

1. Create a new Merge Request in the Gitlab UI with a target branch of `capstone-main`. Have someone else Review, approve, and merge the MR to `capstone-main`.

At this point, a pipeline should run and create your first UI package. It will also trigger an MR on the [One Step UI Project](https://gitlab.com/onestepprojects/one-step-ui). Instructions on how to finish the merge in that project are to follow.



## Additional Resources

* [Basic Git Tutorial](https://www.atlassian.com/git)
* [Interactive Git Branching Tutorial](https://learngitbranching.js.org/?locale=en_US)
* [Publishing multiple distribution channels](https://semantic-release.gitbook.io/semantic-release/recipes/release-workflow/distribution-channels)
* [Angular Commit Message Formatting](https://gist.github.com/brianclements/841ea7bffdb01346392c)
* [Organization Module](https://gitlab.com/onestepprojects/organization-module)
* [Ant Design](https://ant.design/)
